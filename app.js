var express = require('express');
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var youtubeStream = require('youtube-audio-stream');
var ytdl = require('ytdl-core');
var request = require('request');
var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(cookieParser());
app.use('/', express.static('public'));

var router = express.Router();
app.use('/', router);
router.get('/convertomp3/:videoId', function(req, res, next) {
    var requestUrl = 'https://www.youtube.com/watch?v=' + req.params.videoId;
    try {
        youtubeStream(requestUrl).pipe(res)
    } catch (exception) {
        res.status(500).send(exception)
    }
});

router.get('/convertomp4/:videoId', function(req, res, next) {
    var requestUrl = 'https://www.youtube.com/watch?v=' + req.params.videoId;
    try {
        ytdl(requestUrl)
            .pipe(res);
    } catch (exception) {
        res.status(500).send(exception)
    }
});


router.get('/searchYoutube/:q', function(req, res, next) {
    var requestUrl = 'http://suggestqueries.google.com/complete/search?jsonp=&client=youtube&hl=en&ds=yt&q=' + req.params.q ;

    request
        .get(requestUrl)
        .pipe(res)
}
);

module.exports = app;
