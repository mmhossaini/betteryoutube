var App = angular.module('Youtube', [
    'ngMaterial',
    'youtube-embed',
    'angularMoment',
    'ngSanitize'
]);

App.constant('appConfig', {
    defaultVideoDefinition: 'high',
    videoDefinitionTypes: [{
        displayName: 'All',
        type: 'any'
    }, {
        displayName: 'HD',
        type: 'high'
    }, {
        displayName: 'Standard',
        type: 'standard'
    }],
    defaultSortType: 'relevance',
    sortTypes: [{
        displayName: 'Date',
        type: 'date'

    }, {
        displayName: 'Rating',
        type: 'rating'

    }, {
        displayName: 'Relevance',
        type: 'relevance'

    }, {
        displayName: 'Title',
        type: 'title'

    }, {
        displayName: 'Videos',
        type: 'videocount'

    }, {
        displayName: 'Views',
        type: 'viewcount'
    }],
    defaultVideo: '6MJER5s7UYg',
    youtube: {
        key: 'AIzaSyDKnJvpBJXOtY9qJj0K0Xr5YcDOOC1qivI',
        maxResults: 50,
        maxResultsComment: 50
    },
    commentSortType: 'relevance',
    commentSortTypes: [{
        displayName: 'Top comments',
        type: 'relevance'
    }, {
        displayName: 'Newest first',
        type: 'time'
    }]
});

App.config([
    '$mdIconProvider',
    '$mdThemingProvider',
    'appConfig',
    function(
        $mdIconProvider,
        $mdThemingProvider,
        appConfig) {

        $mdIconProvider.iconSet("alert", "components/material-design-icons/sprites/svg-sprite/svg-sprite-alert.svg");
        $mdIconProvider.iconSet("av", "components/material-design-icons/sprites/svg-sprite/svg-sprite-av.svg");
        $mdIconProvider.iconSet("device", "components/material-design-icons/sprites/svg-sprite/svg-sprite-device.svg");
        $mdIconProvider.iconSet("editor", "components/material-design-icons/sprites/svg-sprite/svg-sprite-editor.svg");
        $mdIconProvider.iconSet("file", "components/material-design-icons/sprites/svg-sprite/svg-sprite-file.svg");
        $mdIconProvider.iconSet("hardware", "components/material-design-icons/sprites/svg-sprite/svg-sprite-hardware.svg")
        $mdIconProvider.iconSet("maps", "components/material-design-icons/sprites/svg-sprite/svg-sprite-maps.svg")
        $mdIconProvider.iconSet("notification", "components/material-design-icons/sprites/svg-sprite/svg-sprite-notification.svg")
        $mdIconProvider.iconSet("hardware", "components/material-design-icons/sprites/svg-sprite/svg-sprite-hardware.svg")
        $mdIconProvider.iconSet("social", "components/material-design-icons/sprites/svg-sprite/svg-sprite-social.svg");
        $mdIconProvider.iconSet("action", "components/material-design-icons/sprites/svg-sprite/svg-sprite-action.svg");
        $mdIconProvider.iconSet("communication", "components/material-design-icons/sprites/svg-sprite/svg-sprite-communication.svg");
        $mdIconProvider.iconSet("content", "components/material-design-icons/sprites/svg-sprite/svg-sprite-content.svg");
        $mdIconProvider.iconSet("toggle", "components/material-design-icons/sprites/svg-sprite/svg-sprite-toggle.svg");
        $mdIconProvider.iconSet("navigation", "components/material-design-icons/sprites/svg-sprite/svg-sprite-navigation.svg");
        $mdIconProvider.iconSet("image", "components/material-design-icons/sprites/svg-sprite/svg-sprite-image.svg")

    }
]);

App.controller('MainCtrl', [
    '$scope', '$http', '$mdDialog', '$timeout', 'appConfig', '$q',
    function($scope, $http, $mdDialog, $timeout, appConfig, $q) {
        $scope.videoDefinitionTypes = appConfig.videoDefinitionTypes;
        $scope.videoDefinitionType = appConfig.defaultVideoDefinition;
        $scope.sortTypes = appConfig.sortTypes;
        $scope.sortType = appConfig.defaultSortType;

        // these options are for the virtual reppeater
        $scope.iv = {}
        $scope.iv.numLoaded_ = 0;
        $scope.iv.toLoad_ = 0;
        $scope.iv.nextPageToken = '';
        $scope.videos = [];


        this.infiniteVideos = {
            getItemAtIndex: function(index) {
                if (index > $scope.iv.numLoaded_) {
                    this.fetchMoreVideos_(index);
                    return null;
                }

                return index;
            },
            getLength: function() {
                return $scope.iv.numLoaded_ + (appConfig.youtube.maxResults / 2);
            },

            fetchMoreVideos_: function(index) {
                if ($scope.iv.toLoad_ < index) {
                    $scope.iv.toLoad_ += appConfig.youtube.maxResults;
                    $http.get('https://www.googleapis.com/youtube/v3/search', {
                            params: {
                                key: appConfig.youtube.key,
                                type: 'video',
                                videoDefinition: $scope.videoDefinitionType,
                                disablekb: 0,
                                maxResults: appConfig.youtube.maxResults,
                                videoEmbeddable: true,
                                part: 'id,snippet',
                                pageToken: $scope.iv.nextPageToken,
                                order: $scope.sortType,
                                q: $scope.searchCtrl.searchText
                            }
                          }
                        )
                        .then(function(res) {
                          var data = res.data;
                            $scope.iv.numLoaded_ = $scope.iv.toLoad_;
                            $scope.iv.nextPageToken = data.nextPageToken;
                            angular.forEach(data.items, function(video) {
                                $scope.videos.push(video);
                            });

                            var videoIdArray = $scope.videos.map(function(video) {
                              return video.id.videoId;
                            })


                            var videoDuration=[];
                            $http.get('https://www.googleapis.com/youtube/v3/videos', {
                              params: {
                                key: appConfig.youtube.key,
                                part: 'contentDetails',
                                id: videoIdArray.join()
                              }
                            }).
                            then(function (res) {
                              var items = res.data.items;

                              $scope.videoDetails = items;
                            })


                        })
                }
            }
        };

        $scope.resetComments = function() {
            $scope.infiniteComments.numLoaded_ = 0;
            $scope.comments = [];
            $scope.infiniteComments.nextPageToken = '';
            $scope.infiniteComments.toLoad_ = 0
        }
        $scope.infiniteComments = {}
        $scope.infiniteComments.numLoaded_ = 0;
        $scope.infiniteComments.toLoad_ = 0;
        $scope.infiniteComments.nextPageToken = '';
        $scope.comments = [];
        $scope.commentSortType = appConfig.commentSortType;
        $scope.commentSortTypes = appConfig.commentSortTypes;

        this.infiniteComments = {
            getItemAtIndex: function(index) {
                if (index > $scope.infiniteComments.numLoaded_) {
                    this.fetchMoreComments_(index);
                    return null;
                }

                return index;
            },
            getLength: function() {
                return $scope.infiniteComments.numLoaded_ + (appConfig.youtube.maxResultsComment / 2);
            },

            fetchMoreComments_: function(index) {
                if ($scope.infiniteComments.toLoad_ < index) {
                    $scope.infiniteComments.toLoad_ += appConfig.youtube.maxResultsComment;
                    $http.get('https://www.googleapis.com/youtube/v3/commentThreads', {
                            params: {
                                key: appConfig.youtube.key,
                                maxResults: appConfig.youtube.maxResultsComment,
                                part: 'id,snippet,replies',
                                pageToken: $scope.infiniteComments.nextPageToken,
                                videoId: $scope.videoId,
                                order: $scope.commentSortType

                            }
                        })
                        .then(function(res) {
                          var data = res.data;
                            $scope.infiniteComments.numLoaded_ = $scope.iv.toLoad_;
                            $scope.infiniteComments.nextPageToken = data.nextPageToken;
                            angular.forEach(data.items, function(comment) {
                                $scope.comments.push(comment);
                            });
                        })
                }
            }
        };


        $scope.search = function() {
            $scope.resetVideos();

        }

        $scope.resetVideos = function() {
            $scope.iv.numLoaded_ = 0;
            $scope.videos = [];
            $scope.iv.nextPageToken = '';
            $scope.iv.toLoad_ = 0
        }

        $scope.resetComments = function() {
            $scope.infiniteComments.numLoaded_ = 0;
            $scope.comments = [];
            $scope.infiniteComments.nextPageToken = '';
            $scope.infiniteComments.toLoad_ = 0
        }

        // youtube
        $scope.videoId = appConfig.defaultVideo;
        $scope.youtubePlayerSettings = {
            autoplay: 0,
            controls: 1,
            showinfo: 0,
            rel: 0,
            loop: 0
        }


        $scope.playVideo = function(video) {
            $scope.videoId = video.id.videoId;

            // get comments
            $scope.resetComments();
            $http.get('https://www.googleapis.com/youtube/v3/commentThreads', {
                    params: {
                        key: appConfig.youtube.key,
                        part: 'id,snippet',
                        videoId: $scope.videoId,
                        order: 'relevance'
                    }
                })
                .then(function(res) {
                  var data = res.data;
                    $scope.comments = data.items;
                })
        }

        $scope.$on('youtube.player.ready', function($event, player) {
            // play it again
            $scope.youtubePlayerSettings.autoplay = true;
        });


        $scope.selectedVideos = [];
        $scope.checkVideo = function(video) {
            var indexOfVideo = $scope.selectedVideos.indexOf(video);
            if (indexOfVideo == -1) {
                indexOfVideo = $scope.selectedVideos.push(video);
            } else {
                $scope.selectedVideos.splice(indexOfVideo, 1);
            }
        }

        $scope.isVideoChecked = function(video) {
            if ($scope.selectedVideos.indexOf(video) != -1) {
                return true;
            }
            return false
        }

        $scope.downloadAllMP4 = function() {

            var index;
            for (index = 0; index < $scope.selectedVideos.length; index++) {
                var video = $scope.selectedVideos[index];
                $http.get(
                    '/convertomp4/' + video.id.videoId, {}, {
                        responseType: 'arraybuffer'
                    }
                ).then(function(res) {
                    var headers = res.headers();
                    var blob = new Blob([res.data], {
                        type: headers['content-type']
                    });
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = video.snippet.title + '.mp4';
                    link.click();
                });
            }
        }

        $scope.downloadAllMP3 = function() {

            var index;
            for (index = 0; index < $scope.selectedVideos.length; index++) {
                var video = $scope.selectedVideos[index];
                $http.get(
                    '/convertomp3/' + video.id.videoId, {}, {
                        responseType: 'arraybuffer'
                    }
                ).then(function(res) {
                    var headers = res.headers();
                    var blob = new Blob([res.data], {
                        type: headers['content-type']
                    });
                    var link = document.createElement('a');
                    link.href = window.URL.createObjectURL(blob);
                    link.download = video.snippet.title + '.mp3';
                    link.click();
                });
            }
        }

        $scope.searchCtrl = {
            searchTextChange: function() {},
            selectedItemChange: function() {},
            querySearch: function(query) {


                var def = $q.defer();

                $http.get("/searchYoutube/" + query)
                    .then(function(res) {
                        var data = res.data;

                        data = data.replace('window.google.ac.h(', '')
                        data = data.substring(0, data.length - 1)
                        data = eval(data)[1];

                        def.resolve(data.map(function(state) {
                            return {
                                value: state[0],
                                display: state[0]
                            }
                        }));

                    });

                return def.promise;

            }
        }

    }
]);


App.directive('myEnter', function() {
    return function(scope, element, attrs) {
        element.bind("keydown keypress", function(event) {
            if (event.which === 13) {
                scope.$apply(function() {
                    scope.$eval(attrs.myEnter);
                });

                event.preventDefault();
            }
        });
    };
});

App.controller('ThumbnailCtrl', function($scope, $interval) {
    $scope.hover = false;
    $scope.thumbnail = 0;
    $scope.getThumbnail = function() {
        if ($scope.videos.length > 0) {
            var thisVid = $scope.videos[$scope.$index];
            if (thisVid) {
                if ($scope.hover && $scope.thumbnail > 0) {
                    return 'https://img.youtube.com/vi/' + thisVid.id.videoId + '/' + $scope.thumbnail + '.jpg';
                } else {
                    return thisVid.snippet.thumbnails.default.url;

                }
            }
        }
    }

    var stop = undefined;
    $scope.mouseOver = function() {
        $scope.hover = true;
        stop = $interval(function() {

            if ($scope.thumbnail == 3) {
                $scope.thumbnail = 0;
            }
            $scope.thumbnail++;
        }, 500);
    }

    $scope.mouseLeave = function() {
        $scope.hover = false;
        $scope.thumbnail = 0;

        $interval.cancel(stop);
    }
})
